# -*- coding: utf-8 -*-

import pyautogui
from flask import Flask, jsonify, render_template, request
app = Flask(__name__)
	
@app.route('/oncekiKanal')
def oncekiKanal():
	pyautogui.press("left")
	return ""

@app.route('/sonrakiKanal')
def sonrakiKanal():
	pyautogui.press("right")
	return ""

@app.route('/buyut')
def buyut():
	pyautogui.click(clicks=2, x=500, y=300)
	return ""

@app.route('/standartKalitePlus')
def standartKalitePlus():
	pyautogui.press('q')
	return ""

@app.route('/standartKalite')
def standartKalite():
	pyautogui.press('w')
	return ""

@app.route('/kotaDostu')
def kotaDostu():
	pyautogui.press('e')
	return ""

@app.route('/otomatik')
def otomatik():
	pyautogui.press('r')
	return ""

@app.route('/sesAc')
def sesAc():
	pyautogui.press('volumeup')
	return ""

@app.route('/sesKis')
def sesKis():
	pyautogui.press('volumedown')
	return ""

@app.route('/sesMute')
def sesMute():
	pyautogui.press('volumemute')
	return ""

@app.route('/yenile')
def yenile():
	pyautogui.press('f5')
	return ""
	
@app.route('/press')
def press():
	button = request.args.get('button')
	pyautogui.press(button)
	return ""

@app.route('/')
def index():
	return render_template('deneme.html')
	

if __name__ == '__main__':
	app.run(host='0.0.0.0', debug=True, port=3134)