package com.example.selami.digiturkplaykumanda;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class Ip extends Fragment {

    public Ip(){

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivity().setTitle("Ayarlar");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View v = inflater.inflate(R.layout.fragment_ip, container, false);

        TextView ipAdress = (TextView) v.findViewById(R.id.lblCurrentIp);
        ipAdress.setText(SendRequest.getInstance().getIp());

        Button btnAyarla = (Button) v.findViewById(R.id.btnIpAyarla);
        btnAyarla.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditText newIpText = (EditText) v.findViewById(R.id.txtIpAdress);
                String newIp = newIpText.getText().toString();
                SendRequest.getInstance().setIp(newIp);
            }
        });
        return v;
    }

}
