package com.example.selami.digiturkplaykumanda;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

/**
 * Created by Selami on 11.10.2017.
 */

public class SendRequest {

    private static SendRequest instance = new SendRequest();
    private RequestQueue request;
    private Context context;
    private String ip = "10.3.38.144";

    public static SendRequest getInstance(){
        return instance;
    }

    private SendRequest(){
    }

    public void setContext(Context context){
        if(context != null) {
            this.context = context;
            request = Volley.newRequestQueue(context);
        }
    }

    public void setIp(String ip){
        this.ip = ip;
    }

    public String getIp(){
        return this.ip;
    }

    public void sendRequest(String komut){
        if(request != null) {
            String url = "http://" + getIp() + ":3134/" + komut;

            StringRequest MyStringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                }
            }, new Response.ErrorListener() { //Create an error listener to handle errors appropriately.
                @Override
                public void onErrorResponse(VolleyError error) {
                }
            });


            request.add(MyStringRequest);
        }
    }

}
