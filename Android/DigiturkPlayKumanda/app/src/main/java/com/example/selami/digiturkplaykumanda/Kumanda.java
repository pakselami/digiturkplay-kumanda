package com.example.selami.digiturkplaykumanda;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;


public class Kumanda extends Fragment {

    EditText pressText;

    public Kumanda(){

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_kumanda, container, false);

        pressText = (EditText) v.findViewById(R.id.press);
        addTextChangeListener();

        KumandaClickListener cl = new KumandaClickListener();
        Button button;

        button = (Button) v.findViewById(R.id.btnSonrakiKanal);
        button.setOnClickListener(cl);
        button = (Button) v.findViewById(R.id.btnOncekiKanal);
        button.setOnClickListener(cl);
        button = (Button) v.findViewById(R.id.btnSesAc);
        button.setOnClickListener(cl);
        button = (Button) v.findViewById(R.id.btnSesMute);
        button.setOnClickListener(cl);
        button = (Button) v.findViewById(R.id.btnSesKis);
        button.setOnClickListener(cl);
        button = (Button) v.findViewById(R.id.btnStandartKalitePlus);
        button.setOnClickListener(cl);
        button = (Button) v.findViewById(R.id.btnStandartKalite);
        button.setOnClickListener(cl);
        button = (Button) v.findViewById(R.id.btnKotaDostu);
        button.setOnClickListener(cl);
        button = (Button) v.findViewById(R.id.btnOtomatik);
        button.setOnClickListener(cl);
        button = (Button) v.findViewById(R.id.btnBuyut);
        button.setOnClickListener(cl);
        button = (Button) v.findViewById(R.id.btnYenile);
        button.setOnClickListener(cl);

        v.findViewById(R.id.layoutKumanda).setOnClickListener(cl);

        return v;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivity().setTitle("Kumanda");
    }

    public void pressButton(String str){
        SendRequest.getInstance().sendRequest("press?button=" + str);
    }

    public void addTextChangeListener(){
        pressText.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {
                String komut = pressText.getText().toString();
                if(!komut.equals("")) {
                    pressButton(komut);
                    pressText.getText().clear();
                }
            }

            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
            }
        });

    }

    public void hideKeyboard(){
        if(getActivity().getCurrentFocus()!=null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(getActivity().INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
        }
    }

    class KumandaClickListener implements View.OnClickListener
    {
        @Override
        public void onClick(View view){

            hideKeyboard();

            switch (view.getId()){
                case R.id.btnSonrakiKanal:
                    komutUygula("sonrakiKanal");
                    break;
                case R.id.btnOncekiKanal:
                    komutUygula("oncekiKanal");
                    break;
                case R.id.btnSesAc:
                    komutUygula("sesAc");
                    break;
                case R.id.btnSesMute:
                    komutUygula("sesMute");
                    break;
                case R.id.btnSesKis:
                    komutUygula("sesKis");
                    break;
                case R.id.btnStandartKalitePlus:
                    komutUygula("standartKalitePlus");
                    break;
                case R.id.btnStandartKalite:
                    komutUygula("standartKalite");
                    break;
                case R.id.btnKotaDostu:
                    komutUygula("kotaDostu");
                    break;
                case R.id.btnOtomatik:
                    komutUygula("otomatik");
                    break;
                case R.id.btnBuyut:
                    komutUygula("buyut");
                    break;
                case R.id.btnYenile:
                    komutUygula("yenile");
                    break;
            }
        }

        public void komutUygula(String komut){
            SendRequest.getInstance().sendRequest(komut);
        }
    }
}
