var qualityButtons = {};
var superLigMatchList = [];
var sporExtraMatchList = [];
var isMatch = false
var currentURL = window.location.href;
var pid = 0;
var rid = 0;
var superLigMatches = "Super Lig Aktif Maçlar<br/>";
var sporExtraMatches = "Spor Extra Aktif Maçlar<br/>";
var isMatchListShow = false;

if(currentURL.indexOf("https://www.digiturkplay.com.tr/izle") >= 0){
	isMatch = true;
	url = new URL(currentURL);
	pid = url.searchParams.get("pid");
	rid = url.searchParams.get("rid");
}

var popuphtml = '<div class="popup">  <div class="popuptext" id="myPopup">Spor</div></div>"<div class="popup"> <div class="popuptext" id="myPopup">A Simple Popup</div></div>';

video = document.getElementById( "wrapper" );
video.innerHTML += popuphtml;

var popup = document.getElementById("myPopup");

document.addEventListener('keydown', function(event) {
	if(event.keyCode == BTN_STANDART_KALITE_PLUS && isMatch) { //q
		setDisplayQuality(STANDART_KALITE_PLUS);
	}
	else if(event.keyCode == BTN_STANDART_KALITE && isMatch) { //w
		setDisplayQuality(STANDART_KALITE);
	}
	else if(event.keyCode == BTN_KOTA_DOSTU && isMatch) { //e
		setDisplayQuality(KOTA_DOSTU);
	}
	else if(event.keyCode == BTN_OTOMATIK && isMatch) { //r
		setDisplayQuality(OTOMATIK);
	}
	else if(event.keyCode == BTN_SUPER_LIG) { //s (superlig maclari listesi)
		showSuperLigMatches();
	}
	else if(event.keyCode == BTN_SPOR_EXTRA) { //d (sporextra maclari listesi)
		showSporExtraMatches();
	} else if((event.keyCode - 96) <= 9 && (event.keyCode - 96) >= 0){//numpad rakamlar
		if(matchList == SUPER_LIG)
			openSuperLigMatch(event.keyCode - 96);
		else if(matchList == SPOR_EXTRA)
			openSporExtraMatch(event.keyCode - 96);
	} else if((event.keyCode - 48) <= 9 && (event.keyCode - 48) >= 0){//rakamlar
		if(matchList == SUPER_LIG)
			openSuperLigMatch(event.keyCode - 48);
		else if(matchList == SPOR_EXTRA)
			openSporExtraMatch(event.keyCode - 48);
	}
});

function openSuperLigMatch(matchIndex){
	if(superLigMatchList.length > matchIndex && matchIndex >= 0){
		window.location.replace(superLigMatchList[matchIndex]);
	}
}

function openSporExtraMatch(matchIndex){
	if(sporExtraMatchList.length > matchIndex && matchIndex >= 0){
		window.location.replace(sporExtraMatchList[matchIndex]);
	}
}

function setDisplayQuality(quality){
	if(qualityButtons[quality] != null){
		qualityButtons[quality].click();
	}
}

function showMatches() {
    popup.classList.toggle("show");
	isMatchListShow = !isMatchListShow;
	if (!isMatchListShow)
		matchList = KANAL;
}

function showSuperLigMatches(){
	if(matchList == SUPER_LIG){
		showMatches();
	} else{
		matchList = SUPER_LIG;
		popup.innerHTML = superLigMatches;
		if(!isMatchListShow){
			showMatches();
		}
	}
}

function showSporExtraMatches(){
	if(matchList == SPOR_EXTRA){
		showMatches();
	} else{
		matchList = SPOR_EXTRA;
		popup.innerHTML = sporExtraMatches;
		if(!isMatchListShow){
			showMatches();
		}
	}
}

function kaliteButonlariAyarla(){
	kaliteButonlari = document.getElementsByClassName("col-xs-4");
	for (i = 0; i < kaliteButonlari.length; i++) { 
		var buton = kaliteButonlari[i].getElementsByTagName('button');
		var butonText = buton[0].textContent;
		if(butonText == "Standart Kalite +"){
			qualityButtons[STANDART_KALITE_PLUS] = buton[0];
		}
		else if(butonText == "Standart Kalite"){
			qualityButtons[STANDART_KALITE] = buton[0];
		}
		else if(butonText == "Kota Dostu"){
			qualityButtons[KOTA_DOSTU] = buton[0];
		}
		else if(butonText == "Otomatik"){
			qualityButtons[OTOMATIK] = buton[0];
		}
	}
}

if(isMatch)
	kaliteButonlariAyarla();
	
	get_information("https://www.digiturkplay.com.tr/superlig", function(text) {
	var el = document.createElement( "html" );
	el.innerHTML = text;
	macListesi =  el.getElementsByClassName( "matches col-xs-12" );
	for (i = 0; i < macListesi.length; i++) { 
		butunMaclar = macListesi[i].getElementsByClassName( "row" );
		for (j = 0; j < butunMaclar.length; j++) { 
			var macHref = butunMaclar[j].getElementsByTagName('a');
			if(macHref.length > 0){
				var macId = macHref[0].getAttribute('data-id');
				var macPid = macHref[0].getAttribute('data-pid');
				
				var homeTeam = butunMaclar[j].getElementsByClassName("col-xs-4 home-team");
				var homeTeamName = homeTeam[0].innerHTML;
				var awayTeam = butunMaclar[j].getElementsByClassName("col-xs-4 away-team");
				var awayTeamName = awayTeam[0].innerHTML;
				
				superLigMatches += superLigMatchList.length + ". " + homeTeamName + " - " + awayTeamName + "<br/>";
				superLigMatchList[superLigMatchList.length] = "https://www.digiturkplay.com.tr/izle?pid=" + macPid + "&rid=" + macId;
			}
		}
	}
	popup.innerHTML = superLigMatches;
});

get_information("https://www.digiturkplay.com.tr/sporextra", function(text) {
	var el = document.createElement( "html" );
	el.innerHTML = text;
	macListesi =  el.getElementsByClassName( "matches col-xs-12" );
	for (i = 0; i < macListesi.length; i++) { 
		butunMaclar = macListesi[i].getElementsByClassName( "row" );
		for (j = 0; j < butunMaclar.length; j++) { 
			var macHref = butunMaclar[j].getElementsByTagName('a');
			if(macHref.length > 0){
				var macId = macHref[0].getAttribute('data-id');
				var macPid = macHref[0].getAttribute('data-pid');
				
				var homeTeam = butunMaclar[j].getElementsByClassName("col-xs-4 home-team");
				var homeTeamName = homeTeam[0].innerHTML;
				var awayTeam = butunMaclar[j].getElementsByClassName("col-xs-4 away-team");
				var awayTeamName = awayTeam[0].innerHTML;
				
				sporExtraMatches += sporExtraMatchList.length + ". " + homeTeamName + " - " + awayTeamName + "<br/>";
				sporExtraMatchList[sporExtraMatchList.length] = "https://www.digiturkplay.com.tr/izle?pid=" + macPid + "&rid=" + macId;
			}
		}
	}
});
