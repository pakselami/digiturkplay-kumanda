var channels = [];
var channelsPlace = {};
var qualityButtons = {};
var currentURL = window.location.href;
var currentChannel = 3;
var isChannel = false;
var sure = 0;
var kanal = 0;

document.addEventListener('keydown', function(event) {
	if(event.keyCode == ONCEKI_KANAL && isChannel) { //Left Arrow
		openChannel(getChannelNumber(-1));
	} 
	else if(event.keyCode == SONRAKI_KANAL && isChannel) { //Right Arrow
		openChannel(getChannelNumber(1));
	}
	else if(event.keyCode == BTN_STANDART_KALITE_PLUS && isChannel) { //q
		setDisplayQuality(STANDART_KALITE_PLUS);
	}
	else if(event.keyCode == BTN_STANDART_KALITE && isChannel) { //w
		setDisplayQuality(STANDART_KALITE);
	}
	else if(event.keyCode == BTN_KOTA_DOSTU && isChannel) { //e
		setDisplayQuality(KOTA_DOSTU);
	}
	else if(event.keyCode == BTN_OTOMATIK && isChannel) { //r
		setDisplayQuality(OTOMATIK);
	}
	else if((event.keyCode - 96) <= 9 && (event.keyCode - 96) >= 0 && matchList == KANAL) {
		kanal = 10 * kanal + (event.keyCode - 96);
		sure = 0;
		kanalGirilfdi = true;
	} 
	else if((event.keyCode - 48) <= 9 && (event.keyCode - 48) >= 0 && matchList == KANAL){
		kanal = 10 * kanal + (event.keyCode - 48);
		sure = 0;
		kanalGirilfdi = true;
	}
});

function openChannel(channel){
	if(channel != null){
		videoLink = "https://www.digiturkplay.com.tr/canli-yayin/" + channel;
		window.location.replace(videoLink);
	}
}

function setDisplayQuality(quality){
	if(qualityButtons["stream_" + currentChannel + "_" + quality] != null){
		qualityButtons["stream_" + currentChannel + "_" + quality].click();
	}
}

function getChannelNumber(next){
	if(isChannel){
		currentChannelIndex = channelsPlace[currentChannel];
		currentChannelIndex = (currentChannelIndex + next) % channels.length;
		if(currentChannelIndex < 0)
			currentChannelIndex = currentChannelIndex + channels.length;
		return channels[currentChannelIndex];
	}
}

get_information("https://www.digiturkplay.com.tr/kanallar", function(text) {
    var el = document.createElement( "html" );
	el.innerHTML = text;
	kanallar = el.getElementsByClassName( "channel col-xs-6 col-sm-4 col-md-2" );
	
	for (i = 0; i < kanallar.length; i++) { 
		var dataCid = kanallar[i].getAttribute('data-cid');
		channelsPlace[dataCid] = i;
		channels[i] = dataCid;
	}
});

kaliteButonlari = document.getElementsByClassName("col-xs-3");
for (i = 0; i < kaliteButonlari.length; i++) { 
	var buton = kaliteButonlari[i].getElementsByTagName('button');
	var dataStream = buton[0].getAttribute('data-stream');
	qualityButtons[dataStream] = buton[0];
}

if(currentURL.indexOf("https://www.digiturkplay.com.tr/canli-yayin") >= 0){
	isChannel = true;
	currentChannel = currentURL.substring(currentURL.lastIndexOf("/") + 1, currentURL.length);
}

window.setInterval(function(){
	if(kanal > 0){
		sure = sure + 1;
		if(matchList == KANAL && sure == 2){
			openChannel(getChannelNumber(kanal - channelsPlace[currentChannel]));
		}
	}
}, 1000);